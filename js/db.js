// main file to interact with db when uploading finished data

const mysql = require('mysql');

const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});

// establish connection to DB
db.connect(err =>
{
    if (err) { console.error("An error occurred while trying to connect to the master database"); throw err; }
    console.log("Successfully connected to master database");
});

// DB interaction
// todo
function uploadScores()
{
    return new Promise((resolve, reject) => 
    {
        var sqlQuery;
    });
}

module.exports = { uploadScores: uploadScores };
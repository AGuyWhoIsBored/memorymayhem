console.log("starting memory mayhem backend ...");

// load env vars
require('dotenv').config();

// external package imports
const express = require('express');
const app = express();
const session = require('express-session');
const bodyParser = require('body-parser');

// init db controller
//const db = require('./js/db');

// configure express
app.enable('trust proxy');
app.set('view-engine', 'ejs');
app.use(express.static(__dirname + '/client/build')); // using react static
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ limit: '1mb', extended: false}));

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    name: 's'
}));

console.log("backend server listening on port " + 8080||process.env.PORT);
app.listen(8080||process.env.PORT);

// get requests
app.get('/', (_, res) => res.sendFile(__dirname + '/client/build/index.html'));

// post requests
app.post('/uploadScores', (_, res) => res.json({ hello: "hello world!" }));